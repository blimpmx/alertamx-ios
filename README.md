# Cliente iOS Alerta MX #

Este proyecto es un cliente iOS para los componentes Alerta MX de Blimp.mx. 

## Instalación ##

Clona este repositorio.

Abre el proyecto en Xcode.

Para cambiar la dirección del servidor edita el archivo Api.h

### Bibliotecas ###
* [Magical Record](https://github.com/magicalpanda/MagicalRecord)
* [AFNetworking](https://github.com/AFNetworking/AFNetworking)

## Contacto ##

* Rodolfo Cartas rodolfo@blimp.mx

## Licencia ##
Copyright 2014 CARTAS DIAZ FLORES GURRIA LEAL MARQUEZ y ASOCIADOS SC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
//
//  BLDetailViewController.h
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Alert+Helper.h"

@interface BLDetailViewController : UIViewController

@property (strong, nonatomic) Alert* detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end

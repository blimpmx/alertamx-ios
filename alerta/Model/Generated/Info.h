//
//  Info.h
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/5/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Alert, Area;

@interface Info : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * certainty;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * event;
@property (nonatomic, retain) NSString * headline;
@property (nonatomic, retain) NSString * severity;
@property (nonatomic, retain) NSString * urgency;
@property (nonatomic, retain) NSDate * expires;
@property (nonatomic, retain) NSString * web;
@property (nonatomic, retain) NSString * instruction;
@property (nonatomic, retain) Alert *alert;
@property (nonatomic, retain) NSSet *area;
@end

@interface Info (CoreDataGeneratedAccessors)

- (void)addAreaObject:(Area *)value;
- (void)removeAreaObject:(Area *)value;
- (void)addArea:(NSSet *)values;
- (void)removeArea:(NSSet *)values;

@end

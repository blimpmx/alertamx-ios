//
//  Info.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/5/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Info.h"
#import "Alert.h"
#import "Area.h"


@implementation Info

@dynamic category;
@dynamic certainty;
@dynamic desc;
@dynamic event;
@dynamic headline;
@dynamic severity;
@dynamic urgency;
@dynamic expires;
@dynamic web;
@dynamic instruction;
@dynamic alert;
@dynamic area;

@end

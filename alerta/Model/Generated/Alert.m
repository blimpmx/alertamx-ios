//
//  Alert.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"
#import "Info.h"


@implementation Alert

@dynamic sender;
@dynamic sent;
@dynamic scope;
@dynamic status;
@dynamic msgType;
@dynamic identifier;
@dynamic viewed;
@dynamic info;

@end

//
//  Alert.h
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Info;

@interface Alert : NSManagedObject

@property (nonatomic, retain) NSString * sender;
@property (nonatomic, retain) NSDate * sent;
@property (nonatomic, retain) NSString * scope;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * msgType;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSNumber * viewed;
@property (nonatomic, retain) NSSet *info;
@end

@interface Alert (CoreDataGeneratedAccessors)

- (void)addInfoObject:(Info *)value;
- (void)removeInfoObject:(Info *)value;
- (void)addInfo:(NSSet *)values;
- (void)removeInfo:(NSSet *)values;

@end

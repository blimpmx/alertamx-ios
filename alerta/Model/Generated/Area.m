//
//  Area.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Area.h"
#import "Info.h"


@implementation Area

@dynamic areaDesc;
@dynamic circle;
@dynamic polygon;
@dynamic info;

@end

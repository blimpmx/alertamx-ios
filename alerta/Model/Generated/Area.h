//
//  Area.h
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Info;

@interface Area : NSManagedObject

@property (nonatomic, retain) NSString * areaDesc;
@property (nonatomic, retain) NSString * circle;
@property (nonatomic, retain) NSString * polygon;
@property (nonatomic, retain) Info *info;

@end

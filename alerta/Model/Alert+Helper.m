//
//  Alert+Helper.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert+Helper.h"

@implementation Alert (Helper)

-(BOOL) importSent:(id)data {
    self.sent = [Alert convertDate:data];
    
    return YES;
}

+ (NSDate*) convertDate: (NSString*) fromString
{
    [NSTimeZone resetSystemTimeZone];
    NSLocale *enUSPOSIXLocale;
    NSDateFormatter *sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
    enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
    [sRFC3339DateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz"];
    if( [fromString characterAtIndex:[fromString length]-1] == 'Z' ) {
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    }
    else {
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    }
    
    NSDate *date = [sRFC3339DateFormatter dateFromString:fromString];
    return date;
}

@end

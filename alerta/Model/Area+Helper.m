//
//  Area+Helper.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Area+Helper.h"

@implementation Area (Helper)

-(BOOL)importCircle:(id)data {
    self.circle = [data componentsJoinedByString:@"|"];
    
    return YES;
}

-(BOOL)importPolygon:(id)data {
    self.polygon = [data componentsJoinedByString:@"|"];
    
    return YES;
}

@end

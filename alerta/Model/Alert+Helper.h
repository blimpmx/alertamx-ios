//
//  Alert+Helper.h
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"

@interface Alert (Helper)

+ (NSDate*) convertDate: (NSString*) fromString;

@end

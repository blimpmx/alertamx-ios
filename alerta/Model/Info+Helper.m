//
//  Info+Helper.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Info+Helper.h"
#import "Alert+Helper.h"

@implementation Info (Helper)

-(BOOL) importCategory:(id)data {
    NSArray *categories = (NSArray*) data;
    self.category = [categories objectAtIndex:0];
    
    return YES;
}

-(BOOL) importExpires:(id)data {
    self.expires = [Alert convertDate:data];
    
    return YES;
}

@end

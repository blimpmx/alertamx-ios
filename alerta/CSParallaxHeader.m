//
//  CSParallaxHeader.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/11/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "CSParallaxHeader.h"

@implementation CSParallaxHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma Mapa Delegate
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    mapView.centerCoordinate = userLocation.location.coordinate;
}

@end

//
//  BLAppDelegate.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "BLAppDelegate.h"
#import "API.h"
#import "Alert+Helper.h"
#import "QTouchposeApplication.h"

@implementation BLAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    QTouchposeApplication *qtouchapp = (QTouchposeApplication*) application;
    qtouchapp.alwaysShowTouches = YES;
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"AlertModel"];
    
    NSURL *baseUrl = [NSURL URLWithString:I_BASE_URL];
    [API sharedApi].httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    [API sharedApi].httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    /* Creamos los tabs que van a ser la navegacion de la app*/
    AlertsViewController *masterViewController = [[AlertsViewController alloc] initWithNibName:@"AlertsViewController" bundle:nil];
    _mainNavigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
    
     [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    
    QueHacerViewController *queHacer = [[QueHacerViewController alloc] initWithNibName:@"QueHacerViewController" bundle:nil];
    _queHacerNavigationController = [[UINavigationController alloc] initWithRootViewController:queHacer];
    
    //Tab de Riesgo, falta implementar
    AtlasRiesgoViewController *parallax = [[AtlasRiesgoViewController alloc] initWithNibName:@"AtlasRiesgoViewController" bundle:nil];
    _riesgoNavigationController = [[UINavigationController alloc] initWithRootViewController:parallax];
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:_queHacerNavigationController, _mainNavigationController, _riesgoNavigationController, nil];
    
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];

    UITabBar *tabBar = self.tabBarController.tabBar;
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];

    UITabBarItem *tabBarItem0 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:2];
    
    [tabBarItem0 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                         [UIFont fontWithName:@"Helvetica" size:8.0f], NSFontAttributeName,
                                         nil] forState:UIControlStateNormal];
    [tabBarItem1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                         [UIFont fontWithName:@"Helvetica" size:8.0f], NSFontAttributeName,
                                         nil] forState:UIControlStateNormal];
    [tabBarItem2 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                         [UIFont fontWithName:@"Helvetica" size:8.0f], NSFontAttributeName,
                                         nil] forState:UIControlStateNormal];

    tabBarItem0.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    tabBarItem1.imageInsets = UIEdgeInsetsMake(-2, -2, -2, -2);
    tabBarItem2.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);

    UIImage *whatToDo = [[UIImage imageNamed:@"foot_quehacer_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *whatToDoS = [[UIImage imageNamed:@"foot_quehacer_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [tabBarItem0 setImage:whatToDo];
    [tabBarItem0 setSelectedImage:whatToDoS];
    
    UIImage *alertsImage = [[UIImage imageNamed:@"alertas_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *alertsImageS = [[UIImage imageNamed:@"alertas_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [tabBarItem1 setImage:alertsImage];
    [tabBarItem1 setSelectedImage:alertsImageS];
    
    UIImage *riskImage = [[UIImage imageNamed:@"icono_atlas_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *riskImageS = [[UIImage imageNamed:@"icono_atlas_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [tabBarItem2 setImage:riskImage];
    [tabBarItem2 setSelectedImage:riskImageS];
    
    UIImage *fondoTabBar = [UIImage imageNamed:@"footer.png"];
    [[UITabBar appearance] setBackgroundImage:fondoTabBar];
    
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor colorWithRed:37/255.0 green:62/255.0 blue:135/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateNormal];
    
    UIColor *titleHighlightedColor = [UIColor colorWithRed:78/255.0 green:180/255.0 blue:175/255.0 alpha:1.0];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateHighlighted];
    
    [self.tabBarController setSelectedIndex:1];
    self.window.backgroundColor = [UIColor redColor];
    
    
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"My token is: %@", deviceToken);
    [API sharedApi].apnsToken = [deviceToken description];
    
    [[[API sharedApi] registerApnsToken] start];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}


- (void)fetchAlerta:(void (^)(UIBackgroundFetchResult))completionHandler andAlertId:(NSString *)alertID andMostrarAlerta:(BOOL) mostrar{
    Alert *alert = [Alert MR_findFirstByAttribute:@"identifier" withValue:alertID];
    
    if(alert) {
        if(mostrar) {
           [self alertaNuevaEntrante:alert];
        }
    } else {
        [[[API sharedApi] fetchAlertWithId:alertID andCompleteBlock:^(BOOL wasSuccessful, NSObject *object) {
            Alert *alert = (Alert*) object;
            if(alert) {
                NSLog(@"object %@", alert);
                if(mostrar) {
                    // todo cambiar numero de notificaciones
                    [self alertaNuevaEntrante:alert];
                }
                
                completionHandler(UIBackgroundFetchResultNewData);
            } else {
                completionHandler(UIBackgroundFetchResultNoData);
            }
        }] start] ;
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // lanzar proceso de actualización
    if(application.applicationState == UIApplicationStateInactive) {
        NSLog(@"Recibiendo notificación en estado Inactive");
        //Show the view with the content of the push
        NSString *alertID = [userInfo objectForKey:@"idalert"];
        [self fetchAlerta:completionHandler andAlertId:alertID andMostrarAlerta:TRUE];
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        NSLog(@"Recibiendo notificación en estado  Background");
        //Refresh the local model
        NSString *alertID = [userInfo objectForKey:@"idalert"];
        [self fetchAlerta:completionHandler andAlertId:alertID andMostrarAlerta:false];
    } else {
        NSLog(@"Recibiendo notificación en estado Active");
        //Show an in-app banner
        //Refresh the local model
        NSString *alertID = [userInfo objectForKey:@"idalert"];
        [self fetchAlerta:completionHandler andAlertId:alertID andMostrarAlerta:true];
    }
    
}

- (void)mostrarAlertaNueva:(Alert*) alert {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AlertaNueva" object:self];
    UIAlertView *uialert = [[UIAlertView alloc] initWithTitle:@"Nueva alerta"
                                                    message:@"Nueva alerta"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [uialert show];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [MagicalRecord cleanUp];
}

-(void) alertaNuevaEntrante:(Alert*) alerta {
    AlertaActivaViewController *alertaActiva = [[AlertaActivaViewController alloc] initWithAlert:alerta];
    [self.tabBarController.delegate tabBarController:self.tabBarController
                          shouldSelectViewController:[[self.tabBarController viewControllers] objectAtIndex:1]];
    [self.tabBarController setSelectedIndex:1];
    [_mainNavigationController pushViewController:alertaActiva animated:YES];
}

-(void)vistaQueHacer {
    //Dependiendo del tipo de alerta, agregar a queHacerNavigationController una vista
    [self.tabBarController.delegate tabBarController:self.tabBarController
                          shouldSelectViewController:[[self.tabBarController viewControllers] objectAtIndex:0]];
    [self.tabBarController setSelectedIndex:0];
}

#pragma mark TabBarDelegate
-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    NSArray *tabViewControllers = tabBarController.viewControllers;
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = viewController.view;
    
    if (fromView == toView) {
        return false;
    }
    
    NSUInteger fromIndex = [tabViewControllers indexOfObject:tabBarController.selectedViewController];
    NSUInteger toIndex = [tabViewControllers indexOfObject:viewController];

    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.5
                       options: toIndex > fromIndex ? UIViewAnimationOptionTransitionCrossDissolve : UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
                        if (finished) {
                            tabBarController.selectedIndex = toIndex;
                        }
                    }];
    
    return TRUE;
}

@end

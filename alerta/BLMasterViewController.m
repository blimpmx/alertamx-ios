//
//  BLMasterViewController.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "BLMasterViewController.h"
#import "BLDetailViewController.h"
#import "Alert.h"
#import "Alert+Helper.h"
#import "Info.h"
#import "API.h"

@interface BLMasterViewController () {
    NSMutableArray *_alerts;
}
@end

@implementation BLMasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(alertaNuevaRecibida:)
                                                 name:@"AlertaNueva"
                                               object:nil];
    
    [self fetchAllAlerts];
    // lanzar proceso de actualizacion de objetos
    [self.tableView reloadData];
        
    [[[API sharedApi] fetchAlertsWithCompleteBlock:^(BOOL wasSuccessful, NSObject *object) {
        if(wasSuccessful) {
            [self fetchAllAlerts];
            [self.tableView reloadData];
        }
    }] start];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) alertaNuevaRecibida:(NSNotification *) notification {
    [self fetchAllAlerts];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchAllAlerts {
    NSString *sortKey = @"sent";
    BOOL ascending = NO;
    _alerts = [[Alert MR_findAllSortedBy:sortKey ascending:ascending] mutableCopy];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _alerts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    TVCAlertaInfo *alertaCell = nil;
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//        alertaCell = (TVCAlertaInfo*)[TVCAlertaInfo cellFromNibNamed:@"TVCAlertInfo"];
        [cell.contentView addSubview:alertaCell];
    } else {
        for (UIView *view  in cell.contentView.subviews) {
            if ( [view isKindOfClass:[TVCAlertaInfo class]] ) {
                alertaCell = (TVCAlertaInfo*) view;
            }
        }
    }
    
    
    
    Alert *alert = _alerts[indexPath.row];
    
    [alertaCell configureCellWithAlert:alert];
//    Info *info = [[alert.info allObjects] objectAtIndex:0];
    
//    cell.textLabel.text = [NSString stringWithFormat:@"%i %@", indexPath.row + 1, info.event];
    //cell.textLabel.text = [alert.sent description];
    
    return cell;
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([[segue identifier] isEqualToString:@"showDetail"]) {
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        Alert *alert = _alerts[indexPath.row];
//        [[segue destinationViewController] setDetailItem:alert];
//    }
//}

@end

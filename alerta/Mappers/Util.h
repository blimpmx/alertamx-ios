//
//  Util.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/12/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    urgencia,
    urgenciaDet,
    urgenciaAct,
    severidad,
    severidadDet,
    severidadAct,
    categoriaTrans,
    categoriaImgs
} tipoDiccionario;

@interface Util : NSObject

+(Util*) sharedInstance;

+(NSString *) valorPorDiccionario:(tipoDiccionario)tipo andLlave:(NSString*) llave;

+ (NSString*) fechaPorNSDate:(NSDate*) fecha;

+(NSString*) fechaPorNSDate:(NSDate*) fecha withFormat:(NSString*)formato;

+ (NSString *) leyendaExpiracion:(NSDate *)expira;


@end

//
//  Util.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/12/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Util.h"

@implementation Util

NSString *rutaSeverity;
NSString *rutaSeverityDet;
NSString *rutaSeverityAct;
NSString *rutaUrgency;
NSString *rutaUrgencyDet;
NSString *rutaUrgencyAct;
NSString *rutaCategory;
NSString *rutaCatImgs;
//Diccionarios para configurar las celdas
NSDictionary *urgenciaDic;
NSDictionary *urgenciaDetDic;
NSDictionary *urgenciaActDic;
NSDictionary *severidadDic;
NSDictionary *severidadActDic;
NSDictionary *severidadDetDic;
NSDictionary *cateTransDic;
NSDictionary *cateIMGDic;

+(Util*) sharedInstance {
    static Util* _sharedInstance;
    
    @synchronized(self) {
        if (!_sharedInstance) {
            _sharedInstance = [[Util alloc] init];
            rutaSeverity = [[NSBundle mainBundle] pathForResource:@"SeverityAlert" ofType:@"json"];
            rutaSeverityDet = [[NSBundle mainBundle] pathForResource:@"SeverityDetails" ofType:@"json"];
            rutaSeverityAct = [[NSBundle mainBundle] pathForResource:@"SeverityActive" ofType:@"json"];
            rutaUrgency = [[NSBundle mainBundle] pathForResource:@"Urgency" ofType:@"json"];
            rutaUrgencyAct = [[NSBundle mainBundle] pathForResource:@"UrgencyActive" ofType:@"json"];
            rutaUrgencyDet = [[NSBundle mainBundle] pathForResource:@"UrgencyDetails" ofType:@"json"];
            rutaCategory = [[NSBundle mainBundle] pathForResource:@"Category" ofType:@"json"];
            rutaCatImgs = [[NSBundle mainBundle] pathForResource:@"CategoryIMGS" ofType:@"json"];
            
            NSError *error;
            NSData *dataUrg = [[NSString stringWithContentsOfFile:rutaUrgency
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error]
                               dataUsingEncoding:NSUTF8StringEncoding];
            NSData *dataUrgAct = [[NSString stringWithContentsOfFile:rutaUrgencyAct
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error]
                               dataUsingEncoding:NSUTF8StringEncoding];
            NSData *dataUrgDet = [[NSString stringWithContentsOfFile:rutaUrgencyDet
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error]
                               dataUsingEncoding:NSUTF8StringEncoding];
            NSData *dataSev = [[NSString stringWithContentsOfFile:rutaSeverity
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error]
                               dataUsingEncoding:NSUTF8StringEncoding];
            NSData *dataSevAct = [[NSString stringWithContentsOfFile:rutaSeverityAct
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error]
                               dataUsingEncoding:NSUTF8StringEncoding];
            NSData *dataSevDet = [[NSString stringWithContentsOfFile:rutaSeverityDet
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error]
                               dataUsingEncoding:NSUTF8StringEncoding];
            NSData *dataCat = [[NSString stringWithContentsOfFile:rutaCategory
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error]
                               dataUsingEncoding:NSUTF8StringEncoding];
            NSData *dataCatImg = [[NSString stringWithContentsOfFile:rutaCatImgs
                                                            encoding:NSUTF8StringEncoding
                                                               error:&error]
                                  dataUsingEncoding:NSUTF8StringEncoding];
            
            urgenciaDic = [NSJSONSerialization JSONObjectWithData:dataUrg
                                                               options:NSJSONReadingMutableContainers error:&error];
            urgenciaActDic = [NSJSONSerialization JSONObjectWithData:dataUrgAct
                                                          options:NSJSONReadingMutableContainers error:&error];
            urgenciaDetDic = [NSJSONSerialization JSONObjectWithData:dataUrgDet
                                                          options:NSJSONReadingMutableContainers error:&error];
            severidadDic = [NSJSONSerialization JSONObjectWithData:dataSev
                                                                options:NSJSONReadingMutableContainers error:&error];
            severidadActDic = [NSJSONSerialization JSONObjectWithData:dataSevAct
                                                           options:NSJSONReadingMutableContainers error:&error];
            severidadDetDic = [NSJSONSerialization JSONObjectWithData:dataSevDet
                                                           options:NSJSONReadingMutableContainers error:&error];
            cateTransDic = [NSJSONSerialization JSONObjectWithData:dataCat
                                                                options:NSJSONReadingMutableContainers error:&error];
            cateIMGDic = [NSJSONSerialization JSONObjectWithData:dataCatImg
                                                              options:NSJSONReadingMutableContainers error:&error];
            
        }
    }
    
    return _sharedInstance;
}

+(NSString*) valorPorDiccionario:(tipoDiccionario)tipo andLlave:(NSString *)llave {
    switch (tipo) {
        case urgencia:
            return [urgenciaDic objectForKey:llave];
        case urgenciaDet:
            return [urgenciaDetDic objectForKey:llave];
        case urgenciaAct:
            return [urgenciaActDic objectForKey:llave];
        case severidad:
            return [severidadDic objectForKey:llave];
        case severidadDet:
            return [severidadDetDic objectForKey:llave];
        case severidadAct:
            return [severidadActDic objectForKey:llave];
        case categoriaTrans:
            return [cateTransDic objectForKey:llave];
        case categoriaImgs:
            return [cateIMGDic objectForKey:llave];
        default:
            break;
    }
    return nil;
}

+ (NSString*) fechaPorNSDate:(NSDate*) fecha {
    return [Util fechaPorNSDate:fecha withFormat:@"dd/MM/yy HH:mm"];
}

+ (NSString*) fechaPorNSDate:(NSDate*) fecha withFormat:(NSString*)formato {
    NSDateFormatter *formattedDate = [[NSDateFormatter alloc]init];
    [formattedDate setDateFormat:formato];
    NSString *str = [formattedDate stringFromDate:fecha];
    
//    NSLog(@"Fecha %@", str);
    
    return str;
}

+ (NSString *) leyendaExpiracion:(NSDate *)expira {
    NSCalendar *calendario = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendario setTimeZone:[NSTimeZone defaultTimeZone]];
    NSDateComponents *componentes = [calendario components:NSHourCalendarUnit | NSMinuteCalendarUnit
                                                  fromDate:[NSDate date]
                                                    toDate:expira
                                                   options:0];
    
    //    NSLog(@"Diferencia   entre %@, %@", [NSDate date], info.expires);
    NSString *texto = nil;
    if (componentes.hour > 1) {
        texto = [NSString stringWithFormat:@"Activo por %li horas", (long)componentes.hour];
    } else if (componentes.hour == 1) {
        texto = [NSString stringWithFormat:@"Activo por una hora"];
    } else if (componentes.minute > 1) {
        texto = [NSString stringWithFormat:@"Activo por %li minutos", (long)componentes.minute];
    } else if (componentes.minute == 1) {
        texto = [NSString stringWithFormat:@"Activo por un minuto"];
    } else {
        texto = @"Inactivo";
    }
    
    return texto;
}

@end

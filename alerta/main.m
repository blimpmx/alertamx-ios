//
//  main.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QTouchposeApplication.h"
#import "BLAppDelegate.h"


int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv,
                                     NSStringFromClass([QTouchposeApplication class]),
                                     NSStringFromClass([BLAppDelegate class]));
    }
}

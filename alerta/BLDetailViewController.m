//
//  BLDetailViewController.m
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "BLDetailViewController.h"
#import "Info+Helper.h"

@interface BLDetailViewController ()
- (void)configureView;
@end

@implementation BLDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(Alert*)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        
        Info *info = [[[_detailItem info] allObjects] objectAtIndex:0];

        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

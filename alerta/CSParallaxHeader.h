//
//  CSParallaxHeader.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/11/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface CSParallaxHeader : UICollectionViewCell <MKMapViewDelegate>

@end

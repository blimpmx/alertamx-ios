//
//  AjustesViewController.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/15/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AjustesViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentVIew;

@end

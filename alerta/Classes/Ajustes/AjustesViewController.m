//
//  AjustesViewController.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/15/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "AjustesViewController.h"

@interface AjustesViewController ()

@end

@implementation AjustesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_scrollView setContentSize:CGSizeMake(320, 991)];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [self configurarNavigation];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) configurarNavigation {
    UIButton *botonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    botonSettings.titleLabel.textColor = [UIColor whiteColor];
    [botonSettings setImage:[UIImage imageNamed:@"back_off.png"] forState:UIControlStateNormal];
    [botonSettings setImage:[UIImage imageNamed:@"back_on.png"] forState:UIControlStateSelected];
    botonSettings.frame = CGRectMake(0, 0, 50, 50);
    [botonSettings addTarget:self action:@selector(regresarAlertas) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *botonSpaR = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [botonSpaR setWidth:-18];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:botonSpaR, [[UIBarButtonItem alloc] initWithCustomView:botonSettings], nil];
}

-(void) regresarAlertas {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

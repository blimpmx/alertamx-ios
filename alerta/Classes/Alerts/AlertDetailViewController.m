//
//  AlertDetailViewController.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/8/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "AlertDetailViewController.h"

@interface AlertDetailViewController ()

@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) UINib *headerNib;

@end

@implementation AlertDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.sections = @[@"Song 1"];
        self.headerNib = [UINib nibWithNibName:@"CVCMapa" bundle:nil];
        self.mapaDibujado = NO;
    }
    return self;
}

-(id) initWithAlert:(Alert *)alerta {
    self = [[AlertDetailViewController alloc] initWithNibName:@"AlertDetailViewController" bundle:nil];
    if (self) {
        _alerta = alerta;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _myCollectionView.delegate = self;
    _myCollectionView.dataSource = self;
    
    CSStickyHeaderFlowLayout *layout = (id)_myCollectionView.collectionViewLayout;
    
    if ( [layout isKindOfClass:[CSStickyHeaderFlowLayout class]] ) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(320, 320);
//        layout.disableStickyHeaders = YES;
    }
    
    [self.myCollectionView setContentOffset:CGPointMake(0, 100)];
    
    [_myCollectionView registerNib:[UINib nibWithNibName:@"CVCAlertaDetail" bundle:nil] forCellWithReuseIdentifier:@"alertCell"];
    
     [_myCollectionView registerNib:[UINib nibWithNibName:@"CVCAlertaDetailHeader" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"alertHeader"];
    
    [_myCollectionView registerNib:self.headerNib forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader withReuseIdentifier:@"stickyHeader"];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    [self configurarNavigation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) configurarNavigation {
    UIButton *botonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    botonSettings.titleLabel.textColor = [UIColor whiteColor];
    [botonSettings setImage:[UIImage imageNamed:@"back_off.png"] forState:UIControlStateNormal];
    [botonSettings setImage:[UIImage imageNamed:@"back_on.png"] forState:UIControlStateSelected];
    botonSettings.frame = CGRectMake(0, 0, 50, 50);
    [botonSettings addTarget:self action:@selector(regresarAlertas) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *botonSpaR = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [botonSpaR setWidth:-18];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:botonSpaR,[[UIBarButtonItem alloc] initWithCustomView:botonSettings], nil];
}

-(void) regresarAlertas {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UICollectionViewDelegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.sections count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.sections count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIndentifier = @"alertCell";
    CVCAlertaDetail *cell = (CVCAlertaDetail*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIndentifier forIndexPath:indexPath];
    
    cell.padre = self;
    [cell configurarDetalleAlerta:_alerta];
    _altura = [cell altura];
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableview = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        CVCAlertaDetailHeader *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                  withReuseIdentifier:@"alertHeader"
                                                                         forIndexPath:indexPath];
        
        [cell configurarConAlerta:_alerta];
        
        reusableview = cell;
    }
    
    if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        CVCMapa *rCell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                             withReuseIdentifier:@"stickyHeader"
                                                                                    forIndexPath:indexPath];
        
        Info *informacion = [[_alerta.info allObjects] firstObject];
        Area *area = [[informacion.area allObjects] firstObject];
        
        if (!_mapaDibujado) {
            [rCell agregarElementosAlMapa:area];
            _mapaDibujado = YES;
        }
        
        reusableview =  rCell;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"altura %@ %i", indexPath, _altura);
    // TODO cambiar este tamaño de acuerdo al texto
    return CGSizeMake(320, 900);
}

@end

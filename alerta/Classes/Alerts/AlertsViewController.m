//
//  AlertsViewController.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/7/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "AlertsViewController.h"
#import "AlertaAnimatedHeader.h"
#import "AlertaAnotacion.h"

@interface AlertsViewController () {
    NSMutableArray *_alerts;
    BOOL isInMap;
}

@property (nonatomic, strong) UINib *headerNib;
@property (strong, nonatomic) UIButton *botonPantalla;

@end

@implementation AlertsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.headerNib = [UINib nibWithNibName:@"AlertaStickyHeader" bundle:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    _mapaZonaAlertas.delegate = self;
    
    [self fetchAllAlerts];

    CSStickyHeaderFlowLayout *layout = (id)_collectionView.collectionViewLayout;
    
    if ( [layout isKindOfClass:[CSStickyHeaderFlowLayout class]] ) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(320, 250);
    }
    layout.disableStickyHeaders = YES;
    
    [_collectionView registerNib:[UINib nibWithNibName:@"TVCAlertaInfo" bundle:nil] forCellWithReuseIdentifier:@"alertCell"];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"TVCAlertHeader" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"iosHeader"];

    [_collectionView registerNib:self.headerNib forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader withReuseIdentifier:@"stickyHeader"];
    

   MKCoordinateRegion rgn = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(19.428470, -99.127660), 2000*1000, 4000*1000);
    
    [_mapaZonaAlertas setRegion:rgn animated:YES];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidAppear:(BOOL)animated {
//    NSLog(@"Did appear");
    [self fetchAllAlerts];
    [[self collectionView] reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [self configurarNavigationBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(alertaNuevaRecibida:)
                                                 name:@"AlertaNueva"
                                               object:nil];
    

    
    [[[API sharedApi] fetchAlertsWithCompleteBlock:^(BOOL wasSuccessful, NSObject *object) {
        if(wasSuccessful) {
            [self fetchAllAlerts];
            [[self collectionView] reloadData];
        }
    }] start];
    [self crearPinsOnMap];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) alertaNuevaRecibida:(NSNotification *) notification {
    [self fetchAllAlerts];
    [[self collectionView] reloadData];
}

-(void) configurarNavigationBar {
    _botonPantalla = [UIButton buttonWithType:UIButtonTypeCustom];
    _botonPantalla.titleLabel.textColor = [UIColor whiteColor];
   // [botonMapa setTitle:@"Mapa" forState:UIControlStateNormal];
    [_botonPantalla setImage:[UIImage imageNamed:@"map_view_off.png"] forState:UIControlStateNormal];
    [_botonPantalla setImage:[UIImage imageNamed:@"map_view_on.png"] forState:UIControlStateSelected];
    _botonPantalla.frame = CGRectMake(0, 0, 50, 50);
    [_botonPantalla addTarget:self action:@selector(flipView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *botonSpaR = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [botonSpaR setWidth:-18];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:botonSpaR, [[UIBarButtonItem alloc] initWithCustomView:_botonPantalla],nil];
    
    UIButton *botonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    botonSettings.titleLabel.textColor = [UIColor whiteColor];
    [botonSettings setImage:[UIImage imageNamed:@"settings_off.png"] forState:UIControlStateNormal];
    [botonSettings setImage:[UIImage imageNamed:@"settings_on.png"] forState:UIControlStateSelected];
    botonSettings.frame = CGRectMake(0, 0, 50, 50);
    [botonSettings addTarget:self action:@selector(mostrarConfiguraciones) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *botonSpaL = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [botonSpaL setWidth:-18.0];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:botonSpaL, [[UIBarButtonItem alloc] initWithCustomView:botonSettings], nil];
    
    self.navigationController.navigationBar.translucent = NO;
}

- (void)fetchAllAlerts {
    NSString *sortKey = @"sent";
    BOOL ascending = NO;
    NSFetchRequest *request = [Alert MR_requestAllSortedBy:sortKey ascending:ascending];
    [request setFetchLimit:20];

    _alerts = [[Alert MR_executeFetchRequest:request] mutableCopy];
}

-(void) flipView {
    [self flipViewAnimated];
    [self.view addSubview:_mapView];
    //Configurar Pins
    if (isInMap) {
        [UIView animateWithDuration:0.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            _mapView.alpha = 0.0;
            isInMap = NO;
        } completion:nil ];
    } else {
        [UIView animateWithDuration:0.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            _mapView.alpha = 1.0;
            isInMap = YES;
        } completion:nil ];
    }
    [self cambiarNavigation];
}

-(void) mostrarConfiguraciones {
    AjustesViewController *ajustes = [[AjustesViewController alloc] initWithNibName:@"AjustesViewController" bundle:nil];
    [self.navigationController pushViewController:ajustes animated:YES];
}

-(void) cambiarNavigation {
    if (isInMap) {
        [_botonPantalla setImage:[UIImage imageNamed:@"list_view_off.png"] forState:UIControlStateNormal];
        [_botonPantalla setImage:[UIImage imageNamed:@"list_view_on.png"] forState:UIControlStateSelected];
    } else {
        [_botonPantalla setImage:[UIImage imageNamed:@"map_view_off.png"] forState:UIControlStateNormal];
        [_botonPantalla setImage:[UIImage imageNamed:@"map_view_on.png"] forState:UIControlStateSelected];
    }
}

-(void) flipViewAnimated {
    if (isInMap) {
        [UIView animateWithDuration:0.25 animations:^{
            CATransition *animation = [CATransition animation];
            [animation setDelegate:self];
            [animation setDuration:0.25];
            [animation setType:kCATransitionPush];
            animation.fillMode = kCAFillModeForwards;
            animation.subtype = kCATransitionFromRight;
            [animation setRemovedOnCompletion:NO];
            [self.view.layer addAnimation:animation forKey:@"changeToList"];
        }];
    } else {
        [UIView animateWithDuration:0.25 animations:^{
            CATransition *animation = [CATransition animation];
            [animation setDelegate:self];
            [animation setDuration:0.25];
            [animation setType:kCATransitionPush];
            animation.fillMode = kCAFillModeForwards;
            animation.subtype = kCATransitionFromLeft;
            [animation setRemovedOnCompletion:NO];
            [self.view.layer addAnimation:animation forKey:@"ChangeToMap"];
            ;}
         ];

    }
}

-(void) crearPinsOnMap {
    AlertaAnotacion *annotacion = nil;
    NSMutableArray *todasAnotaciones = [NSMutableArray array];
    
    NSArray *xyAndR = nil;
    NSArray *xAndy = nil;
    
    Info *info = nil;
    Area *area = nil;
    
    for (Alert *alertaP in _alerts) {
        info = [[alertaP.info allObjects] firstObject];
        area = [[info.area allObjects] firstObject];
        if (area.circle) {
            //Obtener el primer circulo
            xyAndR = [area.circle componentsSeparatedByString:@" "];
            xAndy = [[xyAndR firstObject] componentsSeparatedByString:@","];
            CLLocationDegrees cooX = [[xAndy firstObject] doubleValue];
            CLLocationDegrees cooY = [[xAndy objectAtIndex:1] doubleValue];
            CLLocationCoordinate2D coordenadas = CLLocationCoordinate2DMake(cooX, cooY);
            
            annotacion = [[AlertaAnotacion alloc] initWithCoordinate:coordenadas andAlert:alertaP];
        }
        
        if (area.polygon) {
            NSArray *poligonos = [area.polygon componentsSeparatedByString:@"|"];
            NSArray *coosXY = [[poligonos firstObject] componentsSeparatedByString:@" "];
            NSInteger numCoos = [coosXY count]-1;
            double promX = 0;
            double promY = 0;
            NSArray *xsys = nil;
            for (int i = 0; i < numCoos; i++) {
                xsys = [[coosXY objectAtIndex:i] componentsSeparatedByString:@","];
                promX += [[xsys firstObject] doubleValue];
                promY += [[xsys objectAtIndex:1] doubleValue];
            }
            CLLocationCoordinate2D centroide = CLLocationCoordinate2DMake(promX/numCoos, promY/numCoos);
            annotacion = [[AlertaAnotacion alloc] initWithCoordinate:centroide andAlert:alertaP];
        }
    
        NSString *tituloAlerta = [info.event stringByReplacingOccurrencesOfString:@"Aviso de " withString:@""];
        annotacion.title = tituloAlerta;
        
        [_mapaZonaAlertas addAnnotation:annotacion];
        [todasAnotaciones addObject:annotacion];
    }
    
    [_mapaZonaAlertas showAnnotations:todasAnotaciones animated:YES];
}

#pragma mark UICollectionViewDelegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_alerts count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIndentifier = @"alertCell";
    TVCAlertaInfo *cell = (TVCAlertaInfo*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIndentifier forIndexPath:indexPath];

    [cell configureCellWithAlert:[_alerts objectAtIndex:indexPath.row]];
    
    return cell;
}


-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableview = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        TVCAlertHeader *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                          withReuseIdentifier:@"iosHeader"
                                                                 forIndexPath:indexPath];
        reusableview = cell;
    }
    
  if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
    UICollectionReusableView *rCell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                         withReuseIdentifier:@"stickyHeader"
                                                                                forIndexPath:indexPath];
    
    
    Alert *alertaAct = [_alerts firstObject];
    Info *inf = [[alertaAct.info allObjects] firstObject];
    if ([self alertaEstaActiva:inf.expires]) {
      [(AlertaAnimatedHeader*)rCell configurarAlertaActiva:[_alerts firstObject]];
    } else {
      [(AlertaAnimatedHeader*)rCell empezarAnimacion];
    }
    
    reusableview =  rCell;
  }
  return reusableview;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Alert *alertaSeleccionada = [_alerts objectAtIndex:indexPath.row];
    AlertDetailViewController *detalle = [[AlertDetailViewController alloc] initWithAlert:alertaSeleccionada];
    
    [self.navigationController pushViewController:detalle animated:YES];
}




#pragma mark Map Delegate
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    //Add shapes to map
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleRenderer *circuloView = [[MKCircleRenderer alloc] initWithCircle:overlay];
        [circuloView setFillColor:[[UIColor redColor] colorWithAlphaComponent:0.5]];
         return circuloView;
    } else {
        MKPolygonRenderer *poligono = [[MKPolygonRenderer alloc] initWithPolygon:overlay];
        [poligono setFillColor:[[UIColor blueColor] colorWithAlphaComponent:0.5]];
        return poligono;
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[AlertaAnotacion class]]) {
        AlertaAnotacion *annotacion = (AlertaAnotacion*)annotation;
        MKAnnotationView *anntView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"AlertaAnotacion"];
        
        if (!anntView) {
            anntView = annotacion.annotationView;
        } else {
            anntView.annotation = annotation;
        }
        return anntView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    AlertaAnotacion *anotac = (AlertaAnotacion*)view.annotation;
    Alert *selected = anotac.myAlert;
    AlertDetailViewController *detalle = [[AlertDetailViewController alloc] initWithAlert:selected];
    [self.navigationController pushViewController:detalle animated:YES];
}

-(BOOL) alertaEstaActiva:(NSDate *) expiracion {
  NSCalendar *calendario = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
  [calendario setTimeZone:[NSTimeZone defaultTimeZone]];
  NSDateComponents *componentes = [calendario components:NSMinuteCalendarUnit
                                                fromDate:[NSDate date]
                                                  toDate:expiracion
                                                 options:0];
  return componentes.minute > 0;
  
}



@end

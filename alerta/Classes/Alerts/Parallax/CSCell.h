//
//  CSCell.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/9/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *textLbl;


@end

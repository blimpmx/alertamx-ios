//
//  ParallaxController.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/9/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "ParallaxController.h"

@interface ParallaxController ()

@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) UINib *headerNib;

@end

@implementation ParallaxController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.sections = @[
                          @[
                              @"Song 1",
                              ],
                          ];
        
        self.headerNib = [UINib nibWithNibName:@"CSParallaxHeader" bundle:nil];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _myCollectionView.delegate = self;
    _myCollectionView.dataSource = self;
    
    CSStickyHeaderFlowLayout *layout = (id)_myCollectionView.collectionViewLayout;
    
    if ( [layout isKindOfClass:[CSStickyHeaderFlowLayout class]] ) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(320, 500);
    }
    
    [self.myCollectionView setContentOffset:CGPointMake(0, 250)];
    
    self.myCollectionView.scrollsToTop = YES;
    
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"CSCell" bundle:nil] forCellWithReuseIdentifier:@"cvCell"];
    
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"CSCell" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"iosHeader"];
    
    [self.myCollectionView registerNib:self.headerNib
          forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:@"header"];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UICollectionViewDelegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.sections count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.sections[section] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIndentifier = @"cvCell";
    
    NSString *obj = self.sections[indexPath.section][indexPath.row];
    
    CSCell *cell = (CSCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIndentifier forIndexPath:indexPath];
    
    cell.textLbl.text = [NSString stringWithFormat:@"Numero %@", obj];
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableview = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        CSCell *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                          withReuseIdentifier:@"iosHeader"
                                                                 forIndexPath:indexPath];
//        cell.textLbl.text = [NSString stringWithFormat:@"%@", @"Section"];
        reusableview = cell;
    }
    
    if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        NSLog(@"Ahora si es stickyHeader");
        UICollectionReusableView *rCell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                            withReuseIdentifier:@"header"
                                                                                   forIndexPath:indexPath];
        
        reusableview =  rCell;
    }
    return reusableview;
}















@end

//
//  CSCell.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/9/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "CSCell.h"

@implementation CSCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  AlertaAnotacion.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/22/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Alert;

@interface AlertaAnotacion : NSObject <MKAnnotation> 

@property (nonatomic, readonly) Alert *myAlert;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;

- (id) initWithCoordinate:(CLLocationCoordinate2D)coord andAlert:(Alert*) alerta;
-(MKAnnotationView*) annotationView;
@end

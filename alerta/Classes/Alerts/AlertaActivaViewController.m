//
//  AlertaActivaViewController.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/13/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "AlertaActivaViewController.h"
#import "BLAppDelegate.h"
#import "Util.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface AlertaActivaViewController () {
    BOOL alarmaEncendida;
}
@property (strong, nonatomic) NSTimer *timerTiny;

@end

@implementation AlertaActivaViewController

@synthesize alertaEntrante = _alertaEntrante;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id) initWithAlert:(Alert *)alerta {
    self = [[AlertaActivaViewController alloc] initWithNibName:@"AlertaActivaViewController" bundle:nil];
    if (self) {
        _alertaEntrante = alerta;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *beeDoo = [[NSBundle mainBundle] pathForResource:@"BeeDo" ofType:@"mp3"];
    NSURL *alarmaURL = [NSURL fileURLWithPath:beeDoo];
    
    NSError *error;
    alarmPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:alarmaURL error:&error];
    alarmPlayer.numberOfLoops = -1;
    
    if (!error) {
        alarmaEncendida = YES;
        [alarmPlayer play];
    }
    
    _circuloChico.layer.cornerRadius = 65;
    _circuloChico.layer.masksToBounds = YES;
    _circuloChico.layer.borderColor = [UIColor whiteColor].CGColor;
    _circuloChico.layer.borderWidth = 2.f;
    
    [self animarCirculo];
    
    Info *info = [[_alertaEntrante.info allObjects] firstObject];
    NSString *imgnamed = [Util valorPorDiccionario:categoriaImgs andLlave:info.category];
    _imgCategoria.image = [UIImage imageNamed:imgnamed];
    _lblCategoria.text = [Util valorPorDiccionario:categoriaTrans andLlave:info.category];
   
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    [self configurarNavigation];
}

-(void) configurarNavigation {
    UIButton *botonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    botonSettings.titleLabel.textColor = [UIColor whiteColor];
    [botonSettings setImage:[UIImage imageNamed:@"back_off.png"] forState:UIControlStateNormal];
    [botonSettings setImage:[UIImage imageNamed:@"back_on.png"] forState:UIControlStateSelected];
    botonSettings.frame = CGRectMake(0, 0, 50, 50);
    [botonSettings addTarget:self action:@selector(quitarNotificacion) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:botonSettings];
}


-(void) quitarNotificacion {
    [self detenerAlerta:false];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) animarCirculo {
    if (alarmaEncendida) {
        _timerTiny = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                      target:self
                                                    selector:@selector(animarCirculoChico)
                                                    userInfo:nil
                                                     repeats:YES];
    } else {
        [_timerTiny invalidate];
        _circuloChico.alpha = 1;
        _circuloChico.frame = CGRectMake(95, 59, 130, 130);
    }
}

-(void) animarCirculoChico {
    CGRect marco = _circuloChico.frame;
    if (_circuloChico.alpha == 0) {
        marco.origin.x = 95;
        marco.origin.y = 59;
        marco.size.width = 130;
        marco.size.height = 130;
        [UIView animateWithDuration:0.5 animations:^{
            _circuloChico.alpha = 1;
            _circuloChico.frame = marco;
        }];
    } else {
        marco.origin.x = 60;
        marco.origin.y = 24;
        marco.size.width = 200;
        marco.size.height = 200;
        [UIView animateWithDuration:0.5 animations:^{
            _circuloChico.alpha = 0;
            _circuloChico.frame = marco;
        }];
    }
}

-(void) stopAlarm {
    if (alarmPlayer && alarmaEncendida) {
        [alarmPlayer stop];
        alarmaEncendida = NO;
        [self animarCirculo];
    }
}

- (IBAction)detenerAlerta:(id)sender {
    [self stopAlarm];

}

- (IBAction)masInfromacion:(id)sender {
}

- (IBAction)queHacer:(id)sender {
    [self stopAlarm];
    BLAppDelegate *app = (BLAppDelegate*)[UIApplication sharedApplication].delegate;
    [app vistaQueHacer];
}

- (IBAction)compartir:(id)sender {
    [self stopAlarm];
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        [self shareOnService:SLServiceTypeFacebook];
    }
}

- (IBAction)contactoEmergencia:(id)sender {
}

- (void) shareOnService:(NSString*) service {
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:service];

    Info *info = [[_alertaEntrante.info allObjects] firstObject];
    [mySLComposerSheet setInitialText:info.event];
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                NSLog(@"Post Sucessful");
//                [self regresar];
                break;
                
            default:
                break;
        }
    }];
    
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
}

@end

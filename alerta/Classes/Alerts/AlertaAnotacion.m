//
//  AlertaAnotacion.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/22/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "AlertaAnotacion.h"

@implementation AlertaAnotacion

@synthesize coordinate = _coordinate;

- (id) initWithCoordinate:(CLLocationCoordinate2D)coord andAlert:(Alert*)alerta
{
    if (self) {
        _coordinate = coord;
        _myAlert = alerta;
    }
    return self;
}

-(MKAnnotationView*) annotationView {
    MKAnnotationView *anvi = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"AlertaAnotacion"];
    anvi.enabled = YES;
    anvi.canShowCallout =   YES;
    anvi.image = [UIImage imageNamed:@"flecha-sin.png"];
    anvi.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return anvi;
}

@end

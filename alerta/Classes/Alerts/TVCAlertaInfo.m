//
//  TVCAlertaInfo.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/7/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "TVCAlertaInfo.h"
#import "Util.h"

@implementation TVCAlertaInfo

@synthesize evento, descripcion, remitente, fechaExpira, fecha;
@synthesize imgSeveridad, imgUrgencia, imgCategory;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}


-(void) configureCellWithAlert:(Alert *)alerta {
    remitente.text = alerta.sender;
    fecha.text = [Util fechaPorNSDate:alerta.sent];
    
    
    Info *info = [[alerta.info allObjects] objectAtIndex:0];
    NSString *nombre = [info.event stringByReplacingOccurrencesOfString:@"Aviso de " withString:@""];
    evento.text = nombre;

    [descripcion setNumberOfLines:2];
    [descripcion sizeToFit];
    
    CGRect myFrame = descripcion.frame;
    // Resize the frame's width to 280 (320 - margins)
    // width could also be myOriginalLabelFrame.size.width
    myFrame = CGRectMake(myFrame.origin.x, myFrame.origin.y, 152, myFrame.size.height);
    descripcion.frame = myFrame;
   
    
    descripcion.text = info.headline;

    fechaExpira.text = [Util leyendaExpiracion:info.expires];
    
    
    NSString *strSeveridad = [Util valorPorDiccionario:severidad andLlave:info.severity];
    NSString *strUrgencia = [Util valorPorDiccionario:urgencia andLlave:info.urgency];
    NSString *strCategoria = [Util valorPorDiccionario:categoriaImgs andLlave:info.category];
    imgSeveridad.image = [UIImage imageNamed:strSeveridad];
    imgUrgencia.image = [UIImage imageNamed:strUrgencia];
    imgCategory.image = [UIImage imageNamed:strCategoria];
    
    //Falta configurar mas
}
@end

//
//  AlertaAnimatedHeader.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/17/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "AlertaAnimatedHeader.h"
#import "Info.h"
#import "Util.h"
#import <stdlib.h>

@implementation AlertaAnimatedHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) empezarAnimacion {
    _viewAlertActiva.alpha = 0;
    NSInteger randomNumber = arc4random() % 3;
    NSString *nombreImg = nil;
    switch (randomNumber) {
        case 0:
            nombreImg = @"viejita.gif";
            break;
        case 1:
            nombreImg = @"fut.gif";
            break;
        case 2:
            nombreImg = @"regadera.gif";
            break;
        default:
            break;
    }
    _imagenAnimada.image = [UIImage imageNamed:nombreImg];
}

-(void)configurarAlertaActiva:(Alert *)alerta {
    
    _viewAlertActiva.alpha = 1;
    Info *info = [[alerta.info allObjects] firstObject];
    
    NSString *cat = [Util valorPorDiccionario:categoriaTrans andLlave:info.category];
    
    _categoriaLbl.text = cat;
    
    _evento.text = info.event;
    _descripcion.text = info.headline;
    _tiempoActivo.text = [Util leyendaExpiracion:info.expires];
    _sender.text = alerta.sender;
    
    NSLog(@"urgencia %@",[Util valorPorDiccionario:urgenciaAct andLlave:info.urgency]);
    NSLog(@"severidad %@",[Util valorPorDiccionario:severidadAct andLlave:info.severity]);
    
    // Estan al reves urgencia = info.seve    y severidad = info.urgen
    _urgencyImg.image = [UIImage imageNamed:[Util valorPorDiccionario:severidadAct andLlave:info.severity]];
    _severityImg.image = [UIImage imageNamed:[Util valorPorDiccionario:urgenciaAct andLlave:info.urgency]];

}

@end

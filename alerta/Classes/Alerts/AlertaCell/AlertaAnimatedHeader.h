//
//  AlertaAnimatedHeader.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/17/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimatedGIFImageSerialization.h"
#import "Alert.h"

@interface AlertaAnimatedHeader : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imagenAnimada;

//Alerta Activa
@property (strong, nonatomic) IBOutlet UIView *viewAlertActiva;
@property (weak, nonatomic) IBOutlet UILabel *categoriaLbl;
@property (weak, nonatomic) IBOutlet UILabel *evento;
@property (weak, nonatomic) IBOutlet UILabel *descripcion;
@property (weak, nonatomic) IBOutlet UILabel *sender;
@property (weak, nonatomic) IBOutlet UIImageView *urgencyImg;
@property (strong, nonatomic) IBOutlet UILabel *tiempoActivo;
@property (strong, nonatomic) IBOutlet UIImageView *severityImg;


-(void) empezarAnimacion;
-(void) configurarAlertaActiva:(Alert*) alerta;

@end

//
//  AlertDetailViewController.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/8/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"
#import "Area.h"
#import "Info.h"
#import "CVCAlertaDetail.h"
#import "CVCAlertaDetailHeader.h"
#import "CVCMapa.h"
#import "CSStickyHeaderFlowLayout.h"
#import <UIKit/UIKit.h>

@interface AlertDetailViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) Alert *alerta;
@property (strong, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property NSInteger altura;
@property BOOL mapaDibujado;


-(id) initWithAlert:(Alert*) alerta;


@end

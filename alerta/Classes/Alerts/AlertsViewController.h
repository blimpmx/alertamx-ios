//
//  AlertsViewController.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/7/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "CSStickyHeaderFlowLayout.h"
#import "AlertDetailViewController.h"
#import "AjustesViewController.h"
#import "TVCAlertaInfo.h"
#import "TVCAlertHeader.h"
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
#import "API.h"

@interface AlertsViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) IBOutlet MKMapView *mapaZonaAlertas;

@end

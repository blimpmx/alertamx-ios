//
//  TVCAlertaInfo.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/7/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"
#import "Info.h"
#import "AlertsViewController.h"
#import "Util.h"

@interface TVCAlertaInfo : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *evento;
@property (strong, nonatomic) IBOutlet UILabel *descripcion;
@property (strong, nonatomic) IBOutlet UILabel *fechaExpira;
@property (strong, nonatomic) IBOutlet UILabel *remitente;
@property (strong, nonatomic) IBOutlet UILabel *fecha;
@property (strong, nonatomic) IBOutlet UIImageView *imgUrgencia;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeveridad;
@property (strong, nonatomic) IBOutlet UIImageView *imgCategory;

-(void) configureCellWithAlert:(Alert *)alerta;


@end

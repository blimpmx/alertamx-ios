//
//  CVCAlertaDetail.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/11/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "BLAppDelegate.h"
#import "CVCAlertaDetail.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@implementation CVCAlertaDetail

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) configurarDetalleAlerta:(Alert *)alerta {
    
    _alert = alerta;
    
     Info *info = [[_alert.info allObjects] objectAtIndex:0];
    
    [_botonShare setImage:[UIImage imageNamed:@"compartiralerta_on.png"] forState:UIControlStateHighlighted];
    [_botonQueHacer setImage:[UIImage imageNamed:@"quehacer_on.png"] forState:UIControlStateHighlighted];
    
    _categoria.text = [Util valorPorDiccionario:categoriaTrans andLlave:info.category];
    _remitente.text = alerta.sender;
    _fecha.text = [Util fechaPorNSDate:alerta.sent];
    
    
    NSInteger altura = _descripcion.frame.origin.y;
    
    _descripcion.text = info.desc;
    [_descripcion setFont:[UIFont fontWithName:@"TrebuchetMS" size:15]];
    [_descripcion sizeToFit];
    [_descripcion layoutIfNeeded];

    altura += _descripcion.frame.size.height + 5;

    _tituloAreaAfectada.frame = CGRectMake(_tituloAreaAfectada.frame.origin.x,
                                       altura,
                                       _tituloAreaAfectada.frame.size.width, _tituloAreaAfectada.frame.size.height);
    
    altura += _tituloAreaAfectada.frame.size.height;
    
    Area *area = [[info.area allObjects] objectAtIndex:0];
    _areas.text = [area areaDesc];
    [_areas setFont:[UIFont fontWithName:@"TrebuchetMS" size:15]];
    [_areas sizeToFit];
    [_areas layoutIfNeeded];
    _areas.frame = CGRectMake(_areas.frame.origin.x,
                                      altura,
                                      _areas.frame.size.width,
                                      _areas.frame.size.height);

    altura += _areas.frame.size.height + 5;
    
    _tituloAcciones.frame = CGRectMake(_tituloAcciones.frame.origin.x,
                                       altura,
                                       _tituloAcciones.frame.size.width, _tituloAcciones.frame.size.height);
    
    altura += _tituloAcciones.frame.size.height;
    
    _instrucciones.text = info.instruction;
    [_instrucciones setFont:[UIFont fontWithName:@"TrebuchetMS" size:15]];
    [_instrucciones sizeToFit];
    [_instrucciones layoutIfNeeded];
    _instrucciones.frame = CGRectMake(_instrucciones.frame.origin.x,
                                      altura,
                                      _instrucciones.frame.size.width,
                                      _instrucciones.frame.size.height);
    
    altura += _instrucciones.frame.size.height + 5;
    _altura = altura;

    _imgUrgencia.image = [UIImage imageNamed:[Util valorPorDiccionario:urgenciaDet andLlave:info.urgency]];
}

- (IBAction)compartirAlerta:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        [self shareOnService:SLServiceTypeFacebook];
    } else {
        NSLog(@"No tiene facebook");
    }
}

- (IBAction)queHacer:(id)sender {
    BLAppDelegate *app = (BLAppDelegate*)[UIApplication sharedApplication].delegate;
    [app vistaQueHacer];
}

- (IBAction)mostrarEnlace:(id)sender {
    Info *info = [[_alert.info allObjects] objectAtIndex:0];
    NSURL *urlEdron = [NSURL URLWithString:info.web];
    [[UIApplication sharedApplication] openURL:urlEdron];
}

- (void) shareOnService:(NSString*) service {
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:service];
    
    Info *info = [[_alert.info allObjects] firstObject];
    [mySLComposerSheet setInitialText:info.event];
    [mySLComposerSheet addImage:[UIImage imageNamed:@"icon.png"]];
    if(info.web) {
        [mySLComposerSheet addURL:[NSURL URLWithString:info.web]];
    }
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                NSLog(@"Post Sucessful");
                //                [self regresar];
                break;
                
            default:
                break;
        }
    }];
    
    [_padre presentViewController:mySLComposerSheet animated:YES completion:nil];
}

@end

//
//  CVCMapa.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/13/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Area.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface CVCMapa : UICollectionReusableView <MKMapViewDelegate> {
    MKPointAnnotation* anotacion;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapaView;

-(void) agregarElementosAlMapa:(Area*)area;

@end

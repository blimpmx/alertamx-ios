//
//  CVCMapa.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/13/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "CVCMapa.h"

@implementation CVCMapa

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)agregarElementosAlMapa:(Area *)area {
    _mapaView.delegate = self;
    
    NSArray *circulos = [area.circle componentsSeparatedByString:@"|"];
    if ([circulos count] > 0) {
        for (NSString *circulo in circulos) {
            [_mapaView addOverlay:[self dibujarCirculo:circulo]];
        }
    }
    
    NSArray *poligonos = [area.polygon componentsSeparatedByString:@"|"];
    if ([poligonos count] > 0) {
        for (NSString *poligono in poligonos) {
            [_mapaView addOverlay:[self dibujarPoligono:poligono]];
        }
    }

    //ver si es poligono o circulo
    if (area.polygon) {
        NSArray *poligonoIrr = [[poligonos firstObject] componentsSeparatedByString:@" "];
        NSInteger numCoor = [poligonoIrr count] -1;
        double promX = 0;
        double promY = 0;
        NSArray *xsys = nil;
        for (int i = 0; i < numCoor; i++) {
            xsys = [[poligonoIrr objectAtIndex:i] componentsSeparatedByString:@","];
            promX += [[xsys firstObject] doubleValue];
            promY += [[xsys objectAtIndex:1] doubleValue];
        }
        
        CLLocationCoordinate2D centroide = CLLocationCoordinate2DMake(promX/numCoor, promY/numCoor);
        
        MKCoordinateRegion rgn = MKCoordinateRegionMakeWithDistance(centroide, 1000000, 1000000);
        [_mapaView setRegion:rgn];
    }
    
    
}

-(MKCircle*) dibujarCirculo:(NSString*) circulo {
    NSArray *xyAndR = [circulo componentsSeparatedByString:@" "];
    NSArray *xAndy = [[xyAndR firstObject] componentsSeparatedByString:@","];
    CLLocationDegrees cooX = [[xAndy firstObject] doubleValue];
    CLLocationDegrees cooY = [[xAndy objectAtIndex:1] doubleValue];
    CLLocationCoordinate2D coordenadas = CLLocationCoordinate2DMake(cooX, cooY);
    CLLocationDistance radio = [[xyAndR objectAtIndex:1] doubleValue] * 1000;
    
    return [MKCircle circleWithCenterCoordinate:coordenadas radius:radio];
}

-(MKPolygon*) dibujarPoligono:(NSString*) poligono {
    NSArray *xsAndYs = [poligono componentsSeparatedByString:@" "];
    NSArray *arrXY = nil;
    NSInteger numeroDeCoor = [xsAndYs count];
    CLLocationCoordinate2D coor[numeroDeCoor];
    for (int i = 0; i < numeroDeCoor; i++) {
        arrXY = [[xsAndYs objectAtIndex:i] componentsSeparatedByString:@","];
        CLLocationDegrees cooX = [[arrXY firstObject] doubleValue];
        CLLocationDegrees cooY = [[arrXY objectAtIndex:1] doubleValue];
        coor[i] = CLLocationCoordinate2DMake(cooX, cooY);
    }
    return [MKPolygon polygonWithCoordinates:coor count:numeroDeCoor];
}




#pragma mark MKMapViewDelegate
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleRenderer *circuloView = [[MKCircleRenderer alloc] initWithCircle:overlay];
        [circuloView setFillColor:[[UIColor orangeColor] colorWithAlphaComponent:0.5]];
        return circuloView;
    } else {
        MKPolygonRenderer *poligono = [[MKPolygonRenderer alloc] initWithPolygon:overlay];
        [poligono setFillColor:[[UIColor orangeColor] colorWithAlphaComponent:0.5]];
        return poligono;
    }
}


@end

//
//  CVCAlertaDetailHeader.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/11/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"
#import "Info.h"
#import "Util.h"
#import <UIKit/UIKit.h>

@interface CVCAlertaDetailHeader : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *categoria;
@property (strong, nonatomic) IBOutlet UILabel *evento;
@property (strong, nonatomic) IBOutlet UILabel *descripcion;
@property (strong, nonatomic) IBOutlet UILabel *fechaActiva;
@property (strong, nonatomic) IBOutlet UILabel *senderAndMore;

@property (strong, nonatomic) IBOutlet UIImageView *imgSeverity;
@property (strong, nonatomic) IBOutlet UIImageView *imgCategory;

-(void) configurarConAlerta:(Alert*) alerta;

@end

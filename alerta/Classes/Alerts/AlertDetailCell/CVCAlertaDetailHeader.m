//
//  CVCAlertaDetailHeader.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/11/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "CVCAlertaDetailHeader.h"

@implementation CVCAlertaDetailHeader

@synthesize categoria, evento,descripcion;
@synthesize fechaActiva, senderAndMore;
@synthesize imgSeverity, imgCategory;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) configurarConAlerta:(Alert*) alerta {
    
    Info *info = [[alerta.info allObjects] objectAtIndex:0];
    NSString *nombre = [info.event stringByReplacingOccurrencesOfString:@"Aviso de " withString:@""];
    evento.text = nombre;
    [evento sizeToFit];
    [evento layoutIfNeeded];
    
    descripcion.text = info.headline;
    [descripcion sizeToFit];
    [descripcion layoutIfNeeded];
    
    fechaActiva.text = [Util leyendaExpiracion:info.expires];
    
    categoria.text = [Util valorPorDiccionario:categoriaTrans andLlave:info.category];
    NSString *remitente = alerta.sender;
    senderAndMore.text = remitente;
    
    NSString *strSeveridad = [Util valorPorDiccionario:severidadDet andLlave:info.severity];
    NSLog(@"%@", strSeveridad);
    imgSeverity.image = [UIImage imageNamed:strSeveridad];
    NSString *strCategory = [Util valorPorDiccionario:categoriaImgs andLlave:info.category];
    imgCategory.image = [UIImage imageNamed:strCategory];
    
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

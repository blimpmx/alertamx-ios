//
//  CVCAlertaDetail.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/11/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"
#import "Info.h"
#import "Util.h"
#import <UIKit/UIKit.h>

@class AlertDetailViewController;

@interface CVCAlertaDetail : UICollectionViewCell

@property (strong, nonatomic) AlertDetailViewController *padre;
@property (strong, nonatomic) Alert *alert;

@property (strong, nonatomic) IBOutlet UILabel *categoria;
@property (strong, nonatomic) IBOutlet UILabel *remitente;
@property (strong, nonatomic) IBOutlet UILabel *fecha;

@property (strong, nonatomic) IBOutlet UILabel *tituloAcciones;
@property (strong, nonatomic) IBOutlet UILabel *tituloAreaAfectada;

@property (strong, nonatomic) IBOutlet UIImageView *imgUrgencia;
@property (strong, nonatomic) IBOutlet UITextView *descripcion;
@property (strong, nonatomic) IBOutlet UITextView *instrucciones;
@property (strong, nonatomic) IBOutlet UITextView *areas;
@property (strong, nonatomic) IBOutlet UIButton *botonShare;
@property (strong, nonatomic) IBOutlet UIButton *botonQueHacer;

@property NSInteger altura;

-(void) configurarDetalleAlerta:(Alert*) alerta;

- (IBAction)compartirAlerta:(id)sender;
- (IBAction)queHacer:(id)sender;
- (IBAction)mostrarEnlace:(id)sender;
@end

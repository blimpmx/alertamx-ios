//
//  AlertaActivaViewController.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/13/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Alert.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AlertaActivaViewController : UIViewController {
    
    AVAudioPlayer *alarmPlayer;
}

@property (strong, nonatomic) Alert *alertaEntrante;

@property (strong, nonatomic) IBOutlet UIImageView *imgCategoria;
@property (strong, nonatomic) IBOutlet UIImageView *circuloChico;
@property (strong, nonatomic) IBOutlet UILabel *lblCategoria;

-(id) initWithAlert:(Alert*) alerta;

- (IBAction)detenerAlerta:(id)sender;
- (IBAction)masInfromacion:(id)sender;
- (IBAction)queHacer:(id)sender;
- (IBAction)compartir:(id)sender;
- (IBAction)contactoEmergencia:(id)sender;
@end

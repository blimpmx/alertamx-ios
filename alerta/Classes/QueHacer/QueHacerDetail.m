//
//  QueHacerDetail.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/22/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "QueHacerDetail.h"

@interface QueHacerDetail ()

@end

@implementation QueHacerDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configurarNavigation];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) configurarNavigation {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    view.userInteractionEnabled = NO;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"quehacer_sig.png"]];
    image.backgroundColor = [UIColor clearColor];
    [image setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-50, 10, 100, 30)];
    image.contentMode = UIViewContentModeScaleAspectFill;
    //    image.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [view addSubview:image];
    [self.navigationController.navigationBar addSubview:view];
    
    UIButton *botonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    botonSettings.titleLabel.textColor = [UIColor whiteColor];
    [botonSettings setImage:[UIImage imageNamed:@"back_off.png"] forState:UIControlStateNormal];
    [botonSettings setImage:[UIImage imageNamed:@"back_on.png"] forState:UIControlStateSelected];
    botonSettings.frame = CGRectMake(0, 0, 50, 50);
    [botonSettings addTarget:self action:@selector(regresar) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *botonSpaL = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [botonSpaL setWidth:-18.0];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:botonSpaL, [[UIBarButtonItem alloc] initWithCustomView:botonSettings], nil];
}

-(void) regresar {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

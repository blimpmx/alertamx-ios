//
//  QueHacerViewController.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/8/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "Util.h"
#import "TVCTipoEmergencia.h"
#import "QueHacerViewController.h"

@interface QueHacerViewController ()

@property (strong, nonatomic) NSArray *categorias;

@end

@implementation QueHacerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.categorias = [NSArray arrayWithObjects:@"Geo",
                           @"Met",
                           @"Security",@"Rescue",@"Health",
                           @"Env", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) handleSingleTap:(UITapGestureRecognizer *)recognizer {
    QueHacerDetail *detalle = [[QueHacerDetail alloc] initWithNibName:@"QueHacerDetail" bundle:nil];
    [self.navigationController pushViewController:detalle animated:YES];
}

#pragma mark UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.categorias count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *identifier = @"MyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    TVCTipoEmergencia *catCell = nil;
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        catCell = (TVCTipoEmergencia*)[TVCTipoEmergencia cellFromNibNamed:@"TVCTipoEmergencia"];
        [cell.contentView addSubview:catCell];
    } else {
        for (UIView *view in cell.contentView.subviews) {
            if ([view isKindOfClass:[TVCTipoEmergencia class]]) {
                catCell = (TVCTipoEmergencia*) view;
            }
        }
    }
    
    NSString *cat = [self.categorias objectAtIndex:indexPath.row];;
    NSString *strCategoria = [Util valorPorDiccionario:categoriaTrans andLlave:cat];
    NSString *imgByCat = [Util valorPorDiccionario:categoriaImgs andLlave:cat];
    catCell.categoriaLbl.text = strCategoria;
    catCell.categoriaIMG.image = [UIImage imageNamed:imgByCat];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        NSLog(@"Selected row of section >> %ld",indexPath.row);
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"Selected section>> %ld",indexPath.section);
//    NSLog(@"Selected row of section >> %ld",indexPath.row);
//    QueHacerDetail *detalle = [[QueHacerDetail alloc] initWithNibName:@"QueHacerDetail" bundle:nil];
//    [self.navigationController pushViewController:detalle animated:YES];
//
//}

@end

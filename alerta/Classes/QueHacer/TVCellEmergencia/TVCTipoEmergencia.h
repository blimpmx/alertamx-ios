//
//  TVCTipoEmergencia.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/13/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "XIBBasedTableCellTableViewCell.h"
#import <Foundation/Foundation.h>

@interface TVCTipoEmergencia : XIBBasedTableCell

@property (strong, nonatomic) IBOutlet UILabel *categoriaLbl;
@property (strong, nonatomic) IBOutlet UIImageView *categoriaIMG;


@end

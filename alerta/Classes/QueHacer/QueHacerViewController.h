//
//  QueHacerViewController.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/8/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QueHacerDetail.h"

@interface QueHacerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@end

//
//  XIBBasedTableCellTableViewCell.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/7/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "XIBBasedTableCellTableViewCell.h"

@implementation XIBBasedTableCell

+ (XIBBasedTableCell *)cellFromNibNamed:(NSString *)nibName {
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:NULL];
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    XIBBasedTableCell *xibBasedCell = nil;
    NSObject* nibItem = nil;
    
    while ((nibItem = [nibEnumerator nextObject]) != nil) {
        if ([nibItem isKindOfClass:[XIBBasedTableCell class]]) {
            xibBasedCell = (XIBBasedTableCell *)nibItem;
            break; // we have a winner
        }
    }
    
    return xibBasedCell;
}

@end

//
//  AtlasRiesgoViewController.m
//  alerta
//
//  Created by Jose Galindo Martinez on 9/22/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import "AtlasRiesgoViewController.h"

@interface AtlasRiesgoViewController ()

@end

@implementation AtlasRiesgoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configurarNavigationBar];
    
    _mapView.delegate = self;
    
    CLLocationCoordinate2D coordenadasPoligono[9] = { {17.12403354,-100.998230}, {18.275345,-103.971277},
        {19.894895,-105.216980}, {20.060099,-106.6671775}, {21.784026,-105.634460},
        {21.109140,-110.380554}, {15.859942,-105.810241}, {15.690781,-102.030945},
        {17.12403354,-100.998230}};
    
    
    MKPolygon *poligon = [MKPolygon polygonWithCoordinates:coordenadasPoligono count:9];
    
    [_mapView addOverlay:poligon];
    
    MKPointAnnotation *anno = [[MKPointAnnotation alloc] init];
    anno.title = @"Ciclon Tropical";
    anno.coordinate = poligon.coordinate;
    
    [_mapView addAnnotation:anno];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}


-(void) configurarNavigationBar {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    view.userInteractionEnabled = NO;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"titulo_atlasderiesgo.png"]];
    image.backgroundColor = [UIColor clearColor];
    [image setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-90, 5, 180, 30)];
    image.contentMode = UIViewContentModeScaleAspectFill;
//    image.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [view addSubview:image];
    [self.navigationController.navigationBar addSubview:view];
}



#pragma mark MKMapViewDelegate
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    MKPolygonRenderer *poligono = [[MKPolygonRenderer alloc] initWithPolygon:overlay];
    [poligono setFillColor:[[UIColor orangeColor] colorWithAlphaComponent:0.5]];
    return poligono;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
//    MKPolygonView *pol = (MKPolygonView*)[_mapView rendererForOverlay:annotation];
//    return pol;
    return nil;
}

@end

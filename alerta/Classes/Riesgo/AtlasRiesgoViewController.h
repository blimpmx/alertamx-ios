//
//  AtlasRiesgoViewController.h
//  alerta
//
//  Created by Jose Galindo Martinez on 9/22/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface AtlasRiesgoViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end

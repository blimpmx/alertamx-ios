//
//  API.h
//  AlertaMX
//
//  Created by Rodolfo Cartas Ayala on 8/29/14.
//  Copyright (c) 2014 Blimp.MX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Util.h"
#import "AFNetworking.h"

static NSString * const I_BASE_URL = @"http://servidor.blimp.mx:11001";
static NSString * const I_API_REGISTRO_APNS = @"/services/registraDispositivo";
static NSString * const I_API_ALERTS = @"/services/alerts";
static NSString * const I_API_ALERT = @"/services/alert";

typedef void (^APIRequestCompleteBlock) (BOOL wasSuccessful, NSObject *object);

@interface API : NSObject

+ (API *)sharedApi;

@property (strong, nonatomic) AFHTTPClient *httpClient;
@property (strong, nonatomic) NSString *baseUrl;
@property (strong, nonatomic) NSString *apnsToken;

- (AFHTTPRequestOperation*) registerApnsToken;

-(AFHTTPRequestOperation *) fetchAlertsWithCompleteBlock:(APIRequestCompleteBlock)completeBlock;
-(AFHTTPRequestOperation *) fetchAlertWithId:(NSString*)alertId andCompleteBlock:(APIRequestCompleteBlock)completeBlock;

@end

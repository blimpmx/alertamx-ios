//
//  API.m
//  AlertaMX
//
//  Created by Rodolfo Cartas Ayala on 8/29/14.
//  Copyright (c) 2014 Blimp.MX. All rights reserved.
//

#import "API.h"
#import "Alert+Helper.h"

#define I_DEFAULT_TIMEOUT_INTERVAL 30

@interface API (hidden)

- (NSMutableURLRequest*) creaDefaultRequest:(NSString*)method
                                    andPath:(NSString*)path
                                  andParams:(NSDictionary*)params;

- (NSMutableURLRequest*) creaMultipartRequestWithPath:(NSString*)path
                                            andParams:(NSDictionary*)params
                                          andFileData:(NSData*)data
                                          andFileName:(NSString*)filename;

@end

@implementation API

+ (API *)sharedApi {
    static API *_sharedApi;
    
    @synchronized(self)
    {
        if (!_sharedApi) {
            _sharedApi = [[API alloc] init];
            [Util sharedInstance];
        }
        
        return _sharedApi;
    }
}

- (AFHTTPRequestOperation*)  registerApnsToken {
    AFHTTPRequestOperation *operation = nil;
    if (_apnsToken) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];

        NSString *token = [[_apnsToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
        token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [params setObject:token forKey:@"token"];
        [params setObject:@"ios" forKey:@"tecnologia"];
        NSMutableURLRequest *request =[self creaDefaultRequest:@"POST" andPath:I_API_REGISTRO_APNS andParams:params];
        
        operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            NSLog(@"apns añadido");
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            NSLog(@"%@", error);
            NSLog(@"falla en el registro apns");
        }];
    }
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:nil];
    
    return operation;
}

-(AFHTTPRequestOperation *) fetchAlertsWithCompleteBlock:(APIRequestCompleteBlock)completeBlock {
    NSLog(@"fetchAlertsWithCompleteBlock");
    
    NSMutableURLRequest *request = [self creaDefaultRequest:@"POST" andPath:I_API_ALERTS andParams:nil];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSArray *respuestas = (NSArray *)JSON;
        
        NSManagedObjectContext * localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        // guardar las respuestas en la bd
        NSArray *alertas = [Alert MR_importFromArray:respuestas];
        
        //[self.fechasActualizacion setObject:[NSDate date] forKey:@"fechaAlerts"];
        
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error){
            if(completeBlock) {
                completeBlock(success, alertas);
            }
        }];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"ERROR FETCHING ALERTS");
        if(completeBlock) {
            completeBlock(NO, nil);
        }
        
    }];
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:nil];
    
    return operation;
};

-(AFHTTPRequestOperation *) fetchAlertWithId:(NSString*)alertId andCompleteBlock:(APIRequestCompleteBlock)completeBlock {
    NSLog(@"fetchAlertsWithId");
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:alertId forKey:@"id"];
    
     NSMutableURLRequest *request = [self creaDefaultRequest:@"POST" andPath:I_API_ALERT andParams:params];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSDictionary *respuesta = (NSDictionary *)JSON;
        
        NSManagedObjectContext * localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        // guardar la respuesta en la bd
        Alert *alert = [Alert MR_importFromObject:respuesta];
        
        //[self.fechasActualizacion setObject:[NSDate date] forKey:@"fechaAlerts"];
        
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error){
            if(completeBlock) {
                completeBlock(success, alert);
            }
        }];

        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"ERROR FETCHING ALERTS");
        if(completeBlock) {
            completeBlock(NO, nil);
        }
        
    }];
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:nil];
    
    return operation;
}

@end

@implementation API (hidden)

- (NSMutableURLRequest*)creaDefaultRequest:(NSString*)method
                                   andPath:(NSString*)path
                                 andParams:(NSDictionary*)params {
    NSMutableURLRequest *request = [_httpClient requestWithMethod:method path:path parameters:params];
    NSLog(@"Request para %@", request);
    
    [request setTimeoutInterval:I_DEFAULT_TIMEOUT_INTERVAL];
    
    return request;
}

- (NSMutableURLRequest*)creaMultipartRequestWithPath:(NSString*)path
                                           andParams:(NSDictionary*)params
                                         andFileData:(NSData*)data
                                         andFileName:(NSString*)filename {
    
    NSMutableURLRequest *request = [_httpClient multipartFormRequestWithMethod:@"POST" path:path parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"archivo" fileName:@"archivo" mimeType:@"application/octet-stream"];
    }];
    [request setTimeoutInterval:I_DEFAULT_TIMEOUT_INTERVAL];
    
    return request;
}


@end
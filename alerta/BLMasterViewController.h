//
//  BLMasterViewController.h
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVCAlertaInfo.h"

@interface BLMasterViewController : UITableViewController

@end

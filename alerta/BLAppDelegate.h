//
//  BLAppDelegate.h
//  alerta
//
//  Created by Rodolfo Cartas Ayala on 9/4/14.
//  Copyright (c) 2014 blimp.mx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertsViewController.h"
#import "AlertaActivaViewController.h"
#import "AtlasRiesgoViewController.h"
#import "ParallaxController.h"
#import "QueHacerViewController.h"
#import "BLMasterViewController.h"

@interface BLAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *mainNavigationController;
@property (strong, nonatomic) UINavigationController *queHacerNavigationController;
@property (strong, nonatomic) UINavigationController *riesgoNavigationController;


@property (nonatomic, strong) UITabBarController *tabBarController;

-(void) vistaQueHacer;

@end
